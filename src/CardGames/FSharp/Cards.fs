namespace CardGames

module Cards =

    type Rank = 
        | Two   | Three | Four  | Five  | Six   | Seven | Eight | Nine  | Ten
        | Jack  | Queen | King  | Ace   
        static member Values = 
            Union.ExtractCases<Rank>()

    type Ordering =
        | AcesHigh | AcesLow | AcesHighLow
            
    type Color = 
        | Red   | Black | None
        static member Values = 
            Union.ExtractCases<Color>()
        
    type Suit = 
        | Heart | Diamond   | Club  | Spade     
        static member Values = 
            Union.ExtractCases<Suit>()        
                           
    type RankedCard = 
        {
            Suit : Suit 
            Rank : Rank
        }
        static member IsFace rankedCard = 
            match rankedCard.Rank with 
            | Jack | Queen | King -> true
            | _ -> false
                    
        static member Color rankedCard = 
            match rankedCard.Suit with
            | Heart | Diamond -> Red
            | _ -> Black              

    type Card =
        | Ranked of RankedCard
        | Joker
            
    type Deck = 
        {
            Cards : Card list
        }    
        static member Create () = 
            let ranked = [ 
                for s in Suit.Values do 
                    for f in Rank.Values do 
                         yield Ranked({Suit=s; Rank=f}) ]            
            {Cards = ranked}        

        static member AddJokers jokerCount deck =
            let jokers = [for _ in 1..jokerCount do yield Joker]
            {Cards = deck.Cards @ jokers}

        static member Shuffle deck = 
            {Cards = (List.Shuffle deck.Cards)}

        static member Deal deck cardCount = 
            let hand = deck.Cards |> List.take cardCount
            let left = deck.Cards |> List.skip cardCount
            (hand, {Cards = left})



