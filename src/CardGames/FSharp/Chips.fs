namespace CardGames

module Chips =

    [<Measure>] 
    type dollars 
    
    type BasicChip = 
        | White
        | Pink 
        | Red 
        | Blue 
        | Green 
        | Black 
        static member Values = 
            Union.ExtractCases<BasicChip>()

        static member Value chip =    
            match chip with
            | White ->   1.0<dollars>
            | Pink  ->   2.5<dollars>
            | Red   ->   5.0<dollars>            
            | Blue  ->  10.0<dollars>
            | Green ->  25.0<dollars>
            | Black -> 100.0<dollars>
        
    type Chip = 
        | Basic of BasicChip