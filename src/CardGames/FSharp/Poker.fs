namespace CardGames

module Stud =

    open Cards
    open Chips

    type Hand5 = 
        {
        Card1 : Card
        Card2 : Card
        Card3 : Card
        Card4 : Card
        Card5 : Card
        }
        static member CardCount = 5
    
    type Hand7 = 
        {
        Card1 : Card
        Card2 : Card
        Card3 : Card
        Card4 : Card
        Card5 : Card
        Card6 : Card
        Card7 : Card
        }
        static member CardCount = 7

    type Hand = 
        | Five of Hand5
        | Seven of Hand7
        static member CardCount hand =
            match hand with
            | Five _ -> Hand5.CardCount
            | Seven _ -> Hand7.CardCount

    type Player = {
        Id : string    
        Hand : Hand
        Chips : Chip list
    }

    type Dealer = 
        {
            Deck : Deck
        }

    let rec DealHand dealer hand : Dealer * Hand =
        let cards, deck = Hand.CardCount hand |> (Deck.Deal dealer.Deck)
        
        ({dealer with Deck = deck}, hand)


        // static member DealHand deck hand = 
        //     match hand with
        //     | Hand5 h5 ->
        //     | Hand7 h7 -> 


    type Move = 
        | Raise | Fold  | Call  | Check

    type State = {
        Dealer : Dealer
        Players : Player array
    }

        

                
            