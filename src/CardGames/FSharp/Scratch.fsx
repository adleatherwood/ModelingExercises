
#load "Util.fs"
#load "Cards.fs"
#load "Chips.fs"
#load "Poker.fs"

open CardGames.Cards
open CardGames.Chips

let deck = Deck.Create() 
    
if deck.Cards.Length <> 52 then
    failwith "total count mismatch"

deck.Cards
|> List.groupBy (fun c -> match c with | Ranked r -> r.Suit | _ -> failwith "no suit")
|> List.map snd
|> List.iter (fun cards -> 
    if (cards.Length <> 13) then
        failwith "suit count mismatch")

deck.Cards
|> List.groupBy (fun c -> match c with | Ranked r -> r.Rank | _ -> failwith "no rank")
|> List.map snd
|> List.iter (fun cards -> 
    if (cards.Length <> 4) then
        failwith "rank count mismatch")

if deck.Cards
   |> List.filter (fun c -> match c with | Ranked r -> RankedCard.IsFace r | _ -> false)
   |> List.length
   <> 12 then 
    failwith "face count mismatch"

printfn "Werks!"