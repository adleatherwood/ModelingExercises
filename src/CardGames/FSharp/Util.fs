namespace CardGames

module Union = 

    open Microsoft.FSharp.Reflection

    let ExtractCases<'T>() =
        FSharpType.GetUnionCases(typeof<'T>)
        |> Array.map (fun case -> FSharpValue.MakeUnion(case, [||]) :?> 'T)

module Array =

    let Rand = new System.Random()

    let Shuffle a =    
        let copy = Array.copy a
        let max = copy.Length - 1
        for i in 0..max do
            let r = Rand.Next(0, max)        
            let t = copy.[i]            
            copy.[i] <- copy.[r] 
            copy.[r] <- t 
        copy

    let Include expected actual = 
        expected = actual               
        
    let Exclude expected actual = 
        expected <> actual

module List = 
    let Shuffle l =
        l |> List.toArray |> Array.Shuffle |> Array.toList
